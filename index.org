#+TITLE:     ABSOLUTELLY NO WARRANTY
#+AUTHOR:    Osiris Alejandro Gómez
#+EMAIL:     osiux@osiux.com
#+LANGUAGE:  es
#+LINK_HOME: index.html
#+INCLUDE:   header.org
#+DATE:      {{{modification-time(%Y-%m-%d %H:%M)}}}

*** [[file:2021-02-19-no-me-acuerdo-de-nada-dejame-en-pass.org][/no me acuerdo de nada... dejame en =pass=!/]]

*** [[file:2021-02-16-vivir-del-software-libre.org][/Vivir del Software Libre/]]

*** [[file:2021-02-14-tty-music-on-console.org][=tty= /Music On Console/]]

*** [[file:2021-02-09-install-debian-buster-on-thinkpad-t14-gen-1.org][Install Debian Buster on ThinkPad T14 Gen 1]]

*** [[file:2021-02-07-agrandar-la-tarea.org][agrandar la tarea]]

*** [[file:2021-02-05-git-repos2org.org][generar sección =git= mediante =git-repos2org=]]

*** [[file:2021-02-05-ansible-awx-tools.org][Ansible/AWX tools]]

*** [[file:2021-02-03-git-post-commit-changelog.org][git =post-commit= /ChangeLog/]]

*** [[file:2021-02-01-git-log-org-changelog.org][generar /ChangeLog/ en =org-mode= usando =git log=]]

*** [[file:2021-01-29-bookmarks-vs-links.org][bookmarks vs links.txt]]

*** [[file:2021-01-26-disaster-recovery-plan-osiux.org][DIsaster REcovery PLan OSiux]]

*** [[file:2021-01-25-ansible-luks-format-external-usb-disk.org][ansible luks format external usb disk]]

*** [[file:2019-04-22-install-ca-certificate-into-firefox-from-command-line.org][Instalar una *CA* (*Autoridad Certificante*) en un perfil de *Firefox* desde la consola]]

*** [[file:2019-04-21-gitlab-ci-org-mode-publish.org][gitlab-ci org-mode publish]]

*** [[file:2019-04-10-awx-ansible-tower-cli-failed-to-parse-some-of-the-extra-variables.org][AWX Ansible =tower_cli.exceptions.TowerCLIError: failed to parse some of the extra variables=]]

*** [[file:2018-12-11-org-mode-is-back.org][Org-mode is back!]]

*** [[file:2018-08-21-ejecutar-un-binarioexe-de-32bits-en-un-gnulinux-de-64bits-y-resolver-sus-dependencias.org][Ejecutar un =Binario.exe= de 32bits en un /GNU/Linux/ de 64bits y resolver sus dependencias]]

*** [[file:2018-08-15-realizar-un-profile-en-vim-y-deshabilitar-foldexpr-markdown.org][Realizar un /profile/ en =vim= y deshabilitar =Foldexpr_markdown=]]

*** [[file:2018-01-01-reemplazando-orgmode-por-txt-bash-jrnl.org][Reemplazando *Org-mode* por =txt-bash-jrnl=]]

*** [[file:2017-12-04-nuevo-blog-gracias-a-jrnl-y-markdown-styles.org][nuevo blog gracias a =jrnl= y =markdown-styles=]]

*** [[file:2017-07-10-convertir-org-mode-a-markdown-org2md.org][Convertir Org-mode a Markdown =org2md=]]

*** [[file:2017-07-08-instalar-markdown-styles-para-convertir-un-md-en-un-html.org][Instalar =markdown-styles= para convertir un =.md= en un =.html=]]

*** [[file:2017-07-07-ideas-para-utilizar-txt-bash-jrnl-como-un-sistema-de-micro-blog.org][Ideas para utilizar =txt-bash-jrnl= como un sistema de micro blog]]

*** [[file:2017-04-30-percona-live-17-eze.org][Percona Live 17 =SJO -> DFW -> EZE=]]

*** [[file:2017-04-29-percona-live-17-santa-clara.org][Percona Live 17 =santa clara=]]

*** [[file:2017-04-28-percona-live-17-san-francisco.org][Percona Live 17 =san francisco=]]

*** [[file:2017-04-27-percona-live-17-day-three.org][Percona Live 17 =day three= keynotes]]

*** [[file:2017-04-26-percona-live-17-day-two.org][Percona Live 17 =Day Two= Too Many Talks]]

*** [[file:2017-04-25-percona-live-17-day-one.org][Percona Live 17 =Day One= MySQL Load Balancers]]

*** [[file:2017-04-24-percona-live-17-zero-day.org][Percona Live 17 =Zero Day= tutorials]]


** [[file:blog.org][=blog=]]

#+INCLUDE: "years.org"
